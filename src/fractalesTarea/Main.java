package fractalesTarea;

import javax.swing.JFrame;

/**
 *
 * @author nayel
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        VentanaFractales f = new VentanaFractales();
        f.setSize(800,700);
        f.setLocation(100,0);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    
}
