package fractalesTarea;

import java.awt.*;

public class FractalVicsek {

    private final int x = 200;
    private final int limit = 7;

    public void dibujar(Graphics g, int width, int height) {
        System.out.println("Dibujar Vicsek");
        g.setColor(Color.BLACK);
        int w = (width / 2) - (x / 2);
        int h = (height / 2) - (x / 2);
        g.fillRect(w, h, x, x);
        if (limit > 0) {
            drawSquare(w, h, g);
        }
    }

    private void drawSquare(int w, int h, Graphics g) {
        int n = 0;
        g.fillRect(w - (x / 2), h - (x / 2), x / 2, x / 2);
        drawSquare(w - (x / 2), h - (x / 2), g, n, 0, x / 2);
        g.fillRect(w + x, h - (x / 2), x / 2, x / 2);
        drawSquare(w + x, h - (x / 2), g, n, 1, x / 2);
        g.fillRect(w + x, h + x, x / 2, x / 2);
        drawSquare(w + x, h + x, g, n, 2, x / 2);
        g.fillRect(w - (x / 2), h + x, x / 2, x / 2);
        drawSquare(w - (x / 2), h + x, g, n, 3, x / 2);
    }

    private void drawSquare(int w, int h, Graphics g, int n, int origin, int size) {
        if (n == limit) {
            return;
        }
        n++;
        origin = (origin + 2) % 4;
        if (origin != 0) {
            g.fillRect(w - (size / 2), h - (size / 2), size / 2, size / 2);
            drawSquare(w - (size / 2), h - (size / 2), g, n, 0, size / 2);
        }
        if (origin != 1) {
            g.fillRect(w + size, h - (size / 2), size / 2, size / 2);
            drawSquare(w + size, h - (size / 2), g, n, 1, size / 2);
        }
        if (origin != 2) {
            g.fillRect(w + size, h + size, size / 2, size / 2);
            drawSquare(w + size, h + size, g, n, 2, size / 2);
        }
        if (origin != 3) {
            g.fillRect(w - (size / 2), h + size, size / 2, size / 2);
            drawSquare(w - (size / 2), h + size, g, n, 3, size / 2);
        }
    }
}
